import sys
import re
import numpy as np
from sklearn.model_selection import train_test_split

ratings_file = sys.argv[1]
output_path = sys.argv[2]
dataset_name = sys.argv[3]

file_handle = open(ratings_file, 'r')
lines_list = file_handle.readlines()

#my_data = [["u" + re.split("\t|::", line)[0], "i" + re.split("\t|::", line)[1], re.split("\t|::", line)[2]] for line in lines_list[0:]]
my_data = [[val for val in line.strip().split("\t")] for line in lines_list]

arr = np.array(my_data)
for i in range(1, 11):
    np.random.shuffle(arr)
    train, test = train_test_split(arr, test_size=0.4)
    np.savetxt(output_path+"/" + dataset_name + "_train_spl" + str(i) + ".txt", train, delimiter='\t', fmt='%s')
    np.savetxt(output_path+"/" + dataset_name + "_test_spl" + str(i) + ".txt", test, delimiter='\t', fmt='%s')
