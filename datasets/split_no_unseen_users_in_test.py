import sys
import re
import numpy as np
import math
import random
import copy


ratings_file = sys.argv[1]
output_path = sys.argv[2]
dataset_name = sys.argv[3]

file_handle = open(ratings_file, 'r')
lines_list = file_handle.readlines()
#lines_list = [["u" + re.split("\t|::", line)[0], "i" + re.split("\t|::", line)[1], re.split("\t|::", line)[2]] for line in lines_list[0:]]
lines_list = [[val for val in line.strip().split("\t")] for line in lines_list]


full_data_lenght = len(lines_list)
train_size = int(math.ceil(full_data_lenght * 0.6))
test_size = int(math.floor(full_data_lenght * 0.4))

edges_dict_ori = {}

print("before first for")
sys.stdout.flush()
for line in lines_list:
    if edges_dict_ori.get(line[0], None) is None:
        edges_dict_ori[line[0]] = {}
    edges_dict_ori[line[0]][line[1]] = line[2]
print("end first for")
sys.stdout.flush()
for i in range(1, 11):
    train_set, test_set = [], []
    edges_dict = copy.deepcopy(edges_dict_ori)
    for u in edges_dict.keys():
        item = edges_dict[u].items()
        #print(u)
        v = random.choice(list(item))
        #print(v)
        train_set.append((u, v[0], v[1]))
        del edges_dict[u][v[0]]
    print("added one tuple for each u")
    sys.stdout.flush()
    tuples_list = []
    for item in edges_dict.items():
        for v in item[1].items():
            tuples_list.append((item[0], v[0], v[1]))
    print("created list of remaining tuples")
    print("tuples_list size:", len(tuples_list))
    print("train_size", train_size)
    sys.stdout.flush()

    #train_set.extend([x for x in tuples_list if random.random() < 0.6 and ])
    #test_set.extend([x for x in tuples_list if x not in train_set])

    random.shuffle(tuples_list)
    amount_to_add_to_train = train_size-len(train_set)
    train_set.extend(tuples_list[:amount_to_add_to_train])
    test_set.extend(tuples_list[amount_to_add_to_train:])

    print("added the remaining tuples to train or test set")
    sys.stdout.flush()

    random.shuffle(train_set)
    random.shuffle(test_set)

    np.savetxt(output_path+"/" + dataset_name + "_train_no_unseen_spl" + str(i) + ".txt", train_set, delimiter='\t', fmt='%s')
    np.savetxt(output_path+"/" + dataset_name + "_test_no_unseen_spl" + str(i) + ".txt", test_set, delimiter='\t', fmt='%s')

