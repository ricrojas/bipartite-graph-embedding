import sys
import re
import numpy as np

def build_metrics_dict(mlist, metric, metrics_dict):
    for value in mlist:
        #print(value)
        for topn in [5, 10, 20]:
            if (metrics_dict[metric].get(topn, None) is None):
                metrics_dict[metric][topn] = []
            line_m = re.sub(
                r"(^(?!.* %d).*)\n*" % topn,
                "", 
                value
            )
            line_m = re.sub(
                r".* \d{1,2} : (.*)$",
                "\\1", 
                line_m
            )
            if (line_m):
                metrics_dict[metric][topn].append(float(line_m.strip()))

def calc_avg_metrics(metrics_dict):
    for (metric, mdict) in metrics_dict.items():
        for (topn, mlist) in mdict.items():
            if (len(mlist) > 0):
                avg_metrics_dict[topn][metric] = sum(mlist)/len(mlist)
                std_metrics_dict[topn][metric] = np.std(mlist)

#file containng all the outputs from evaluate.py for an experiment
file_with_outputs_from_eval = sys.argv[1]

with open(file_with_outputs_from_eval, 'r') as file:
    data = file.readlines()

f1_list = []
map_list = []
mrr_list = []
ndcg_list = []
cov_list = []
arp_list = []
aplt_list = []
eild_list = []


i = 0
for line in data:
    line_f1 = re.sub(
        r"(^(?!F1).*)\n*",
        "", 
        line
    )
    if (line_f1):
        f1_list.append(line_f1)
        
    line_map = re.sub(
        r"(^(?!MAP).*)\n",
        "", 
        line
    )
    if (line_map):
        map_list.append(line_map)
        
    line_mrr = re.sub(
        r"(^(?!MRR).*)\n",
        "", 
        line
    )
    if (line_mrr):
        mrr_list.append(line_mrr)
        
    line_ndcg = re.sub(
        r"(^(?!NDCG).*)\n",
        "", 
        line
    )
    if (line_ndcg):
        ndcg_list.append(line_ndcg)
        
    line_cov = re.sub(
        r"(^(?!Coverage).*)\n",
        "", 
        line
    )
    if (line_cov):
        cov_list.append(line_cov)
        
    line_arp = re.sub(
        r"(^(?!ARP).*)\n",
        "", 
        line
    )
    if (line_arp):
        arp_list.append(line_arp)
        
    line_aplt = re.sub(
        r"(^(?!APLT).*)\n",
        "", 
        line
    )
    if (line_aplt):
        aplt_list.append(line_aplt)
        
    line_eild = re.sub(
        r"(^(?!EILD).*)\n",
        "", 
        line
    )
    if (line_eild):
        eild_list.append(line_eild)
      
    i += 1

metrics_dict = {}
metrics_dict['F1'] = {}
metrics_dict['MAP'] = {}
metrics_dict['MRR'] = {}
metrics_dict['NDCG'] = {}
metrics_dict['Coverage'] = {}
metrics_dict['ARP'] = {}
metrics_dict['APLT'] = {}
metrics_dict['EILD'] = {}
build_metrics_dict(f1_list, "F1", metrics_dict)
build_metrics_dict(map_list, "MAP", metrics_dict)
build_metrics_dict(mrr_list, "MRR", metrics_dict)
build_metrics_dict(ndcg_list, "NDCG", metrics_dict)
build_metrics_dict(cov_list, "Coverage", metrics_dict)
build_metrics_dict(arp_list, "ARP", metrics_dict)
build_metrics_dict(aplt_list, "APLT", metrics_dict)
build_metrics_dict(eild_list, "EILD", metrics_dict)


avg_metrics_dict = {}
std_metrics_dict = {}
avg_metrics_dict[5], avg_metrics_dict[10], avg_metrics_dict[20] = {}, {}, {}
std_metrics_dict[5], std_metrics_dict[10], std_metrics_dict[20] = {}, {}, {}


calc_avg_metrics(metrics_dict)


for (topn, mdict) in avg_metrics_dict.items():
    for (metric, avg) in mdict.items():
        print("%s@%d : %.4f +/- %.4f" % (metric, topn, avg, std_metrics_dict[topn][metric]))
