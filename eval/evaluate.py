import sys
from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter
import numpy as np
from data_utils import DataUtils
import os
import pandas as pd
import math
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.metrics import average_precision_score,auc,precision_recall_fscore_support
from collections import Counter

def link_prediction(args):
    filecase_a = args.case_train
    filecase_e = args.case_test
    filevector_u = args.vectors_u
    filevector_v = args.vectors_v
    filecase_a_c = r'../data/features_train.dat'
    filecase_e_c = r'../data/features_test.dat'
    generateFeatureFile(filecase_a,filevector_u,filevector_v,filecase_a_c,args.d)
    generateFeatureFile(filecase_e,filevector_u,filevector_v,filecase_e_c,args.d)

    df_data_train = pd.read_csv(filecase_a_c,header = None,sep='\t',encoding='utf-8')
    X_train = df_data_train.drop(len(df_data_train.keys())-1,axis = 1)
    y_train = df_data_train[len(df_data_train.keys())-1]

    df_data_test = pd.read_csv(filecase_e_c,header = None,sep='\t',encoding='utf-8')
    X_test = df_data_test.drop(len(df_data_train.keys())-1,axis = 1)
    X_test = X_test.fillna(X_test.mean())
    y_test = df_data_test[len(df_data_test.keys())-1]
    y_test_list = list(y_test)

    lg = LogisticRegression(penalty='l2',C=0.001)
    lg.fit(X_train,y_train)
    lg_y_pred_est = lg.predict_proba(X_test)[:,1]
    fpr,tpr,thresholds = metrics.roc_curve(y_test,lg_y_pred_est)
    average_precision = average_precision_score(y_test, lg_y_pred_est)
    os.remove(filecase_a_c)
    os.remove(filecase_e_c)
    return metrics.auc(fpr,tpr), average_precision

#for APLT
def head(dataset_popularity, head_popularity_percentage = 0.5):
    #print([tuple[1] for tuple in dataset_popularity])
    dataset_popularity = dataset_popularity.most_common()
    popularity_total = sum(tuple[1] for tuple in dataset_popularity)
    popularity_sum = 0
    i = 0
    while popularity_sum < popularity_total * head_popularity_percentage:
        popularity_sum = popularity_sum + dataset_popularity[i][1]
        #popularity_sum = 100
        i += 1
    head = []
    head.extend(item[0] for item in dataset_popularity[0:i])
    #print(head)
    return head

#for EILD
def build_ratings_dict(train_users, test_users, train_items, test_items, train_ratings, test_ratings):
    all_users = train_users[:]
    all_users.extend(test_users)
    all_items = train_items[:]
    all_items.extend(test_items)
    all_ratings = train_ratings[:]
    all_ratings.extend(test_ratings)

    ratings_dict = {}
    for i in range(0, len(all_users)):
        if ratings_dict.get(all_users[i], None) is None:
            ratings_dict[all_users[i]] = {}
        ratings_dict[all_users[i]][all_items[i]] = all_ratings[i]
    return ratings_dict

#train_file, test_file -> the train and test data files
#test_u, test_v -> sets of users and items; test_rate -> dict. of ratings
#node_list_u, node_list_v -> embedding vectors of u and v
def calc_new_metrics(train_file, test_file, test_u, test_v, test_rate, node_list_u, node_list_v, top_n):
    recommend_dict = {}
    train_users, train_items, train_ratings = read_file(train_file)
    test_users, test_items, test_ratings = read_file(test_file)
    train_data_popularity = t_popularity(train_items)
    test_data_popularity = t_popularity(test_items)
    dataset_popularity = Counter(train_data_popularity)
    dataset_popularity.update(test_data_popularity)

    ratings_dict = build_ratings_dict(train_users, test_users, train_items, test_items, train_ratings, test_ratings)
    
    short_head = head(dataset_popularity)

    #estimate the preferene of every user on every item
    for u in test_u:
        recommend_dict[u] = {}
        for v in test_v:
            if node_list_u.get(u) is None:
                pre = 0
            else:
                U = np.array(node_list_u[u]['embedding_vectors'])
                if node_list_v.get(v) is None:
                    pre = 0
                else:
                    V = np.array(node_list_v[v]['embedding_vectors'])
                    pre = U.dot(V.T)[0][0]
            recommend_dict[u][v] = float(pre)
    
    #for eild: dictionary of distances between items
    distance_dict_items = {}
    for i_k in test_v:
        distance_dict_items[i_k] = {}
        for i_l in test_v:
            if node_list_v.get(i_k) is None:
                pre = 0
            else:
                U = np.array(node_list_v[i_k]['embedding_vectors'])
                if node_list_v.get(i_l) is None:
                    pre = 0
                else:
                    V = np.array(node_list_v[i_l]['embedding_vectors'])
                    pre = 1 - (U.dot(V.T)[0][0] / (np.linalg.norm(U) * np.linalg.norm(V)))
            distance_dict_items[i_k][i_l] = float(pre)

    #print(distance_dict_items)
    
    eild_dict = {}
    coverage_dict = {}
    arp_dict = {}
    aplt_dict = {}
    coverage_dict = {}
    precision_dict = {}
    recall_dict = {}
    f1_dict = {}
    map_dict = {}
    mrr_dict = {}
    mndcg_dict = {}


    unique_t = set(test_v)

    tops = [10,15,20,30, 40, 50, 100]
    top = [value for value in tops if value <= top_n]

    for topn in top:
        #print("top_n: ", top_n)
        #sys.stdout.flush()
        rp_list = []
        plt_list = []

        ratings_dict_rec = {}

        unique_r = set()
        unique_t_u = set()

        precision_list = []
        recall_list = []
        ap_list = []
        ndcg_list = []
        rr_list = []

        for u in test_u:
            tmp_r = sorted(recommend_dict[u].items(), lambda x, y: cmp(x[1], y[1]), reverse=True)[0:min(len(recommend_dict[u]),topn)]
            tmp_t = sorted(test_rate[u].items(), lambda x, y: cmp(x[1], y[1]), reverse=True)[0:min(len(test_rate[u]),len(test_rate[u]))]
            tmp_r_list = [] #list of top n recommended items for user u
            tmp_t_list = [] #list of top n items rated by user u in the test split

            if ratings_dict_rec.get(u, None) is None:
                ratings_dict_rec[u] = {}

            for (item, rate) in tmp_r:
                tmp_r_list.append(item)
                ratings_dict_rec[u][item] = ratings_dict[u].get(item, 1) #eild: if user hasn't rated the item, assume 1? or indifference rating? if i use here the value of indifference rating, problems
                unique_r.add(item)
            #print(tmp_r_list)
            #print(ratings_dict_rec)
            for (item, rate) in tmp_t:
                tmp_t_list.append(item)
   
            pre, rec = precision_and_recall(tmp_r_list,tmp_t_list)
            ap = AP(tmp_r_list,tmp_t_list)
            rr = RR(tmp_r_list,tmp_t_list)
            ndcg = nDCG(tmp_r_list,tmp_t_list)
            precision_list.append(pre)
            recall_list.append(rec)
            ap_list.append(ap)
            rr_list.append(rr)
            ndcg_list.append(ndcg)
            
            rp_list.append(rec_popularity(train_data_popularity, tmp_r_list)) #recommendation popularity
            plt_list.append(plt(short_head, tmp_r_list)) #percentage of long tail items
        
        precision_dict[topn] = sum(precision_list) / len(precision_list)
        recall_dict[topn] = sum(recall_list) / len(recall_list)
        #print(precison, recall)
        #print("plt_list", plt_list)
        #print("precision_list", precision_list)
        #print("recall_list", recall_list)
        #print("precision_dict[topn]", precision_dict[topn], ", recall_dict[topn]", recall_dict[topn])
        f1_dict[topn] = 2 * precision_dict[topn] * recall_dict[topn] / (precision_dict[topn] + recall_dict[topn])
        map_dict[topn] = sum(ap_list) / len(ap_list)
        mrr_dict[topn] = sum(rr_list) / len(rr_list)
        mndcg_dict[topn] = sum(ndcg_list) / len(ndcg_list)

        ###print("topn:", topn, unique_r)
        print("topn:", topn, "len(unique_r):", len(unique_r))
        coverage_dict[topn] = float(len(unique_r)) / len(unique_t) #catalog coverage
        eild_dict[topn] = calc_eild(distance_dict_items, ratings_dict_rec, indiff = 0.5)
        #print(ratings_dict_rec)
        arp_dict[topn] = sum(rp_list) / len(test_u)
        aplt_dict[topn] = sum(plt_list) / len(test_u)

    return f1_dict, map_dict, mrr_dict, mndcg_dict, coverage_dict, arp_dict, aplt_dict, eild_dict

#for EILD
def calc_eild(distance_dict_items, ratings_dict_rec, indiff = 0):
    eild = 0
    g, C, disc, rel, C_k = cal_eild_vars(ratings_dict_rec, indiff)
    
    
    for u in ratings_dict_rec.keys():
        k = 1
        for i_k in ratings_dict_rec[u].keys():
            l = 1
            for i_l in ratings_dict_rec[u].keys():
                #print ("k:", k, "l:", l)
                #print ("i_k:", i_k, "i_l:", i_l)
                
                if k == l:
                    l += 1
                    continue
              
                dist_i_l_k = distance_dict_items[i_k][i_l]#(ratings_dict_rec[u].keys()[k], ratings_dict_rec[u].keys()[l])
                eild += C_k[u][k] * calc_disc(k) * disc[l][k] * rel[u][k] * rel[u][l] * dist_i_l_k
                
                l += 1
            k += 1
    
    #print (C_k)
    #, C, disc, rel, Ck
    return eild/len(ratings_dict_rec.keys())

def cal_eild_vars(ratings_dict_rec, d = 0):
    #print("enters cal_eild_vars")
    g, C, rel, C_k = {}, {}, {}, {}
    #disc is a dictionary of discount values for keys l,k
    disc = calc_disc_t(max([len(ratings_dict_rec[user].values()) for user in ratings_dict_rec.keys()]))   #len(longest in dict_rec))

    for u in ratings_dict_rec.keys():
        g[u] = calc_g(ratings_dict_rec[u].values(), d)
        C[u] = calc_C(len(ratings_dict_rec[u].values()))
        rel[u] = calc_rel(ratings_dict_rec[u].values(), g[u])
        C_k[u] = calc_C_k(C[u], ratings_dict_rec[u], disc, rel[u])
    return g, C, disc, rel, C_k

def calc_C_k(C_u, ratings, disc, rel_u):
    C_k = {}
    for k in range(1, len(ratings)+1):
        sum = 0
        for l in range(1, len(ratings)+1):
            if k == l:
                continue
            sum += (disc[l][k] * rel_u[l])
        C_k[k] = (float(C_u) / sum)
    return C_k

def calc_rel(ratings, g_u):
    rel = {}
    g_max = max(g_u.values())
    k = 1
    for item in ratings:
        rel[k] = calc_rel_i(g_u[k], g_max)
        k += 1
    return rel

def calc_rel_i(g_i, g_max):
    return (float(pow(2, g_i)-1)/pow(2, g_max))

def calc_disc_t(n):
    disc_dict = {}
    k = 1
    for k in range(1, n+1):
        disc_dict[k] = {}
        l = 1
        for l in range(1, n+1):
            if k == l:
                continue
            disc_dict[k][l] = calc_discount_l_k(l, k)
    return disc_dict

def calc_discount_l_k(l, k):
    return calc_disc(max(1, l-k))

def calc_C(k):
    C_inv = 0
    for i in range (1,k+1):
        C_inv += calc_disc(i)
    return (1/C_inv)

def calc_disc(k):
    return 1/math.log(k+1,2)

def calc_g(ratings, d):
    i = 1
    g_u = {}
    for rating in ratings:
        g_u[i] = max(0, rating-d)
        i += 1
    #if (g_u is not None):
    #    print("not none")
    return g_u

#for APLT
def plt(short_head, tmp_r_list):
    items_in_long_tail = 0
    for item in tmp_r_list:
        if not item in short_head:
            items_in_long_tail += 1
    return float(items_in_long_tail) / len(tmp_r_list)

def t_popularity(t_items):
    return Counter(t_items)

#for ARP
def rec_popularity(train_data_popularity, tmp_r_list):
    popularity = 0
    for item in tmp_r_list:
        popularity += train_data_popularity[item]
    return float(popularity) / len(tmp_r_list)



def nDCG(ranked_list, ground_truth):
    dcg = 0
    idcg = IDCG(len(ground_truth))
    for i in range(len(ranked_list)):
        id = ranked_list[i]
        if id not in ground_truth:
            continue
        rank = i+1
        dcg += 1/ math.log(rank+1, 2)
    return dcg / idcg

def IDCG(n):
    idcg = 0
    for i in range(n):
        idcg += 1 / math.log(i+2, 2)
    return idcg

def AP(ranked_list, ground_truth):
    hits, sum_precs = 0, 0.0
    for i in range(len(ranked_list)):
        id = ranked_list[i]
        if id in ground_truth:
            hits += 1
            sum_precs += hits / (i+1.0)
    if hits > 0:
        return sum_precs / len(ground_truth)
    else:
        return 0.0

def RR(ranked_list, ground_list):

    for i in range(len(ranked_list)):
        id = ranked_list[i]
        if id in ground_list:
            return 1 / (i + 1.0)
    return 0

def precision_and_recall(ranked_list,ground_list):
    hits = 0
    for i in range(len(ranked_list)):
        id = ranked_list[i]
        if id in ground_list:
            hits += 1
    pre = hits/(1.0 * len(ranked_list))
    rec = hits/(1.0 * len(ground_list))
    return pre, rec


def read_vectors(file_vector):
    node_dict = {}
    file_vector = open(file_vector, 'r')
    lines_list = file_vector.readlines()
    node_list = [line.strip().split(" ", 1) for line in lines_list]
    for node in node_list:
        node_dict[node[0]] = {}
        node_dict[node[0]]['embedding_vectors'] = np.array([[float(value) for value in node[1].split()]])
    return node_dict

def read_file(t_file):
    t_data = open(t_file, 'r')
    t_data = t_data.readlines()
    t_users = [line.strip().split("\t")[0] for line in t_data]
    t_items = [line.strip().split("\t")[1] for line in t_data]
    t_ratings = [int(line.strip().split("\t")[2]) for line in t_data]
    return t_users, t_items, t_ratings

def evaluate(args):
    dul = DataUtils("dis dont matter")
    node_list_u = read_vectors(args.vectors_u)
    node_list_v = read_vectors(args.vectors_v)

    if args.rec:
        print("============== testing ===============")
        test_user, test_item, test_rate = dul.read_data(args.test_data)
        f1, map, mrr, mndcg, coverage, arp, aplt, eild = calc_new_metrics(args.train_data, args.test_data, test_user, test_item, test_rate, node_list_u, node_list_v, args.top_n)

        for key in coverage:
            print("F1@ %d : %0.4f" % (key, round(f1[key],4)))
            print("MAP@ %d : %0.4f" % (key, round(map[key],4)))
            print("MRR@ %d : %0.4f" % (key, round(mrr[key],4)))
            print("NDCG@ %d : %0.4f" % (key, round(mndcg[key],4)))
            print("Coverage@ %d : %0.4f" % (key, round(coverage[key],4)))
            print("ARP@ %d : %0.4f" % (key, round(arp[key],4)))
            print("APLT@ %d : %0.4f" % (key, round(aplt[key],4)))
            print("EILD@ %d : %0.4f" % (key, round(eild[key],4)))
    if args.lip:
        print("============== testing ===============")
        auc_roc, auc_pr = link_prediction(args)
        print('link prediction metrics: AUC_ROC : %0.4f, AUC_PR : %0.4f' % (round(auc_roc,4), round(auc_pr,4)))



def main():
    parser = ArgumentParser("BiNE",
                            formatter_class=ArgumentDefaultsHelpFormatter,
                            conflict_handler='resolve')

    parser.add_argument('--train-data', default=r'../data/rating_train.dat',
                        help='Input graph file.')

    parser.add_argument('--test-data', default=r'../data/rating_test.dat')

    parser.add_argument('--vectors-u', default=r'../data/vectors_u.dat',
                        help="file of embedding vectors of U")

    parser.add_argument('--vectors-v', default=r'../data/vectors_v.dat',
                        help="file of embedding vectors of V")

    parser.add_argument('--case-train', default=r'../data/wiki/case_train.dat',
                        help="file of training data for LR")

    parser.add_argument('--case-test', default=r'../data/wiki/case_test.dat',
                        help="file of testing data for LR")

    parser.add_argument('--d', default=128, type=int,
                        help='embedding size.')

    parser.add_argument('--top-n', default=10, type=int,
                        help='recommend top-n items for each user.')

    parser.add_argument('--rec', default=0, type=int,
                        help='calculate the recommendation metrics.')

    parser.add_argument('--lip', default=0, type=int,
                        help='calculate the link prediction metrics.')

    args = parser.parse_args()
    print("parsed args")
    evaluate(args)

if __name__ == "__main__":
    sys.exit(main())
