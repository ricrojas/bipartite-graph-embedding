See https://github.com/clhchtcjj/BiNE/tree/master/data/wiki to know how to run the original code.  


Use eval/evaluate.py to evaluate the embeddings on Link Prediction and Top-k Recommendation:  

-Top 10 recommendation  
F1  
NDCG  
MAP  
MRR  
ARP  
APLT  
Catalog coverage  
EILD (vargas, castells recsys '11, sect. 7.2)  

-Link prediction  
AUC-ROC  
AUC-PR  

****
RUN for recommendation  
python evaluate.py --train-data  ../datasets/dblp/ratings_train.dat --test-data  ../datasets/dblp/ratings_test.dat --vectors-u vectors_u.dat --vectors-v vectors_v.dat --rec 1 --top-n 10
